package com.ppl.fikkrip.itrip.controller.activity;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.ppl.fikkrip.itrip.R;
import com.ppl.fikkrip.itrip.sharedpreference.SessionManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

public class AddPlanningActivity extends AppCompatActivity {

    private Button btnCancel, btnSave;
    private ImageButton ibTanggal;
    private EditText etJudul, etTanggal;
    private int mYear, mMonth, mDay;
    public String idUser, idPlanning, judul, tglPlanning;
    SessionManager session;
    RequestQueue requestQueue;
    StringRequest stringRequest;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_planning);

        btnSave = (Button) findViewById(R.id.btn_save);
        btnCancel = (Button) findViewById(R.id.btn_cancel);
        etJudul = (EditText) findViewById(R.id.et_judul);
        etTanggal = (EditText) findViewById(R.id.et_tanggal);
        ibTanggal = (ImageButton) findViewById(R.id.ib_tanggal);

        Calendar calNow = Calendar.getInstance();
        final Calendar calSet = (Calendar) calNow.clone();
        session = new SessionManager(this);
        session.checkLogin();
        HashMap<String, String> user = session.getUserDetails();
        idUser = user.get(SessionManager.KEY_ID);

        ibTanggal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar c = Calendar.getInstance();
                mYear = c.get(Calendar.YEAR);
                mMonth = c.get(Calendar.MONTH);
                mDay = c.get(Calendar.DAY_OF_MONTH);
                DatePickerDialog datePickerDialog = new DatePickerDialog(AddPlanningActivity.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        etTanggal.setText(year + "-" + (monthOfYear + 1) + "-" + dayOfMonth);
                        calSet.set(Calendar.YEAR, year);
                        calSet.set(Calendar.MONTH, monthOfYear);
                        calSet.set(Calendar.DATE, dayOfMonth);
                    }
                }, mYear, mMonth, mDay);
                datePickerDialog.show();
            }
        });

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final ProgressDialog progressDialog = new ProgressDialog(AddPlanningActivity.this);
                progressDialog.setTitle("Please Wait");
                progressDialog.show();
                progressDialog.setMessage("Processing...");
                judul = etJudul.getText().toString();
                tglPlanning = etTanggal.getText().toString();

                String url = getString(R.string.api) + "SavePlanning.php";

                requestQueue = Volley.newRequestQueue(AddPlanningActivity.this);
                stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonResponse = new JSONObject(response);
                            boolean success = jsonResponse.getBoolean("success");

                            if (success) {
                                progressDialog.dismiss();
                                Toast.makeText(getApplicationContext(), "Success!", Toast.LENGTH_SHORT).show();
                                finish();
                            } else {
                                progressDialog.dismiss();
                                AlertDialog.Builder builder = new AlertDialog.Builder(AddPlanningActivity.this);
                                builder.setMessage("Error!!!")
                                        .setNegativeButton("Retry", null)
                                        .create()
                                        .show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(AddPlanningActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }){
                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {
                        Map<String, String> params = new HashMap<>();
                        params.put("idUser", idUser);
                        params.put("judul", judul);
                        params.put("tglPlanning", tglPlanning);
                        return params;
                    }
                };
                requestQueue.add(stringRequest);
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
