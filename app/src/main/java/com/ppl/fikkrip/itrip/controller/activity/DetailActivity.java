package com.ppl.fikkrip.itrip.controller.activity;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.ppl.fikkrip.itrip.R;
import com.ppl.fikkrip.itrip.model.ReviewModel;
import com.ppl.fikkrip.itrip.sharedpreference.SessionManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

public class DetailActivity extends AppCompatActivity {
    private RequestQueue requestQueue;
    private StringRequest stringRequest;
    private ArrayList<ReviewModel> listReview = null;
    private RecyclerView lvhape;
    private ListReviewAdapter adapter;
    private SessionManager session;
    private String idUser;
    private TextView jmReview;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        jmReview = (TextView) findViewById(R.id.jmlReview);

        session = new SessionManager(getApplicationContext());
        session.checkLogin();
        HashMap<String, String> user = session.getUserDetails();
        idUser = user.get(SessionManager.KEY_ID);
        listReview = new ArrayList<>();

        Intent myIntent = getIntent();
        final Bundle myData = myIntent.getExtras();

        getReview(myData.getInt("idWisata"));

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(myData.getString("namaWisata"));

        TextView tvBiaya = (TextView) findViewById(R.id.tvBiaya);
        final TextView tvLokasi = (TextView) findViewById(R.id.tvLokasi);
        TextView tvDeskripsi = (TextView) findViewById(R.id.tvDeskripsi);
        ImageButton toolbarBack = (ImageButton) findViewById(R.id.toolbar_back);
        FloatingActionButton btns = (FloatingActionButton) findViewById(R.id.fab);
        ImageView imageView = (ImageView) findViewById(R.id.image);
        lvhape = (RecyclerView) findViewById(R.id.lvhape);
        ImageView btnComment = (ImageView) findViewById(R.id.cmnt);

        tvBiaya.setText(Integer.toString(myData.getInt("biayaMasuk")));
        tvLokasi.setText(myData.getString("lokasiWisata"));
        tvDeskripsi.setText(myData.getString("deskripsiWisata"));
        Glide.with(this).load(getString(R.string.img) + myData.getString("gambarWisata")).into(imageView);

        btns.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent sharingIntent = new Intent(Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                sharingIntent.putExtra(Intent.EXTRA_SUBJECT, "I-Trip");
                sharingIntent.putExtra(Intent.EXTRA_TEXT, "Wisata : "+myData.getString("namaWisata")+" Lokasi : "+myData.getString("lokasiWisata")+" (https://www.google.com/maps/place/"+myData.getDouble("longtitude")+","+myData.getDouble("latitude")+")");
                startActivity(Intent.createChooser(sharingIntent, "Share via"));

            }
        });
        toolbarBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        btnComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CommentAdd(myData.getInt("idWisata"));
            }
        });

        LinearLayoutManager llm = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        lvhape.setLayoutManager(llm);
    }
    public void getReview(final int id){
        requestQueue = Volley.newRequestQueue(DetailActivity.this);
        String urls = DetailActivity.this.getString(R.string.api) + "getReviewWisata.php";
        stringRequest = new StringRequest(Request.Method.POST, urls, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    JSONArray jsonArray = jsonObject.getJSONArray("listReview");
                    for (int a = 0; a < jsonArray.length(); a++) {
                        JSONObject json = jsonArray.getJSONObject(a);
                        ReviewModel reviewModel = new ReviewModel();
                        reviewModel.setIdWisata(json.getInt("idWisata"));
                        reviewModel.setIdReview(json.getInt("idReview"));
                        reviewModel.setIdUser(json.getInt("idUser"));
                        reviewModel.setUsername(json.getString("username"));
                        reviewModel.setTglReview(json.getString("tglReview"));
                        reviewModel.setUlasan(json.getString("ulasan"));
                        listReview.add(reviewModel);
                        adapter = new ListReviewAdapter(getApplicationContext(), listReview);
                        lvhape.setAdapter(adapter);
                    }
                    jmReview.setText("( "+listReview.size()+" )");
                } catch (JSONException e) {
                    Toast.makeText(DetailActivity.this, "Data tidak Ada", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(DetailActivity.this, "Error", Toast.LENGTH_SHORT).show();
            }
        }){
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("idWisata", Integer.toString(id));
                return params;
            }
        };
        requestQueue.add(stringRequest);
    }


    private void CommentAdd(final int id){
        final Dialog dialog = new Dialog(this);
        LayoutInflater inflater = DetailActivity.this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.form_komentar, null);
        dialog.setContentView(dialogView);
        dialog.setTitle("Add Your Review :");

        final EditText txt_review = (EditText) dialogView.findViewById(R.id.komentar);
        Button button_save = (Button) dialogView.findViewById(R.id.btn_save);
        Button button_cancel = (Button) dialogView.findViewById(R.id.btn_cancel);

        button_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveKomentar(id, txt_review.getText().toString());
                Toast.makeText(DetailActivity.this, "Review ditambahkan!", Toast.LENGTH_SHORT).show();
                dialog.dismiss();
            }
        });
        button_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }
    public void saveKomentar(final int id, final String ulasan){
        requestQueue = Volley.newRequestQueue(this);
        String urls = this.getString(R.string.api) + "saveReview.php";
        stringRequest = new StringRequest(Request.Method.POST, urls, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonResponse = new JSONObject(response);
                    boolean success = jsonResponse.getBoolean("success");
                    JSONObject j = null;

                    if (success) {
                        Toast.makeText(DetailActivity.this, "Success!", Toast.LENGTH_SHORT).show();
                        listReview.clear();
                        getReview(id);
                    } else {
                        Toast.makeText(DetailActivity.this, "Failed!", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(DetailActivity.this, "Error", Toast.LENGTH_SHORT).show();
            }
        }){
            protected Map<String, String> getParams() throws AuthFailureError {
                Calendar calendar = Calendar.getInstance();
                String tgl = calendar.get(Calendar.YEAR)+"-"+calendar.get(Calendar.MONTH)+"-"+calendar.get(Calendar.DATE);
                Map<String, String> params = new HashMap<>();
                params.put("idWisata", Integer.toString(id));
                params.put("ulasan", ulasan);
                params.put("tgl", tgl);
                params.put("idUser", idUser);
                return params;
            }
        };
        requestQueue.add(stringRequest);
    }
    public class ListReviewAdapter extends RecyclerView.Adapter<ListReviewAdapter.ViewHolder> {

        Context c;
        private ArrayList<ReviewModel> mArrayList;

        public ListReviewAdapter(Context con, ArrayList<ReviewModel> arrayList) {
            this.c = con;
            mArrayList = arrayList;
        }

        @Override
        public ListReviewAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_review, null);
            return new ListReviewAdapter.ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(final ListReviewAdapter.ViewHolder viewHolder, final int i) {
            viewHolder.txtUsername.setText(mArrayList.get(i).getUsername());
            viewHolder.txtUlasan.setText(mArrayList.get(i).getUlasan());
            if(idUser.equals(Integer.toString(mArrayList.get(i).getIdUser()))){
                viewHolder.trash.setVisibility(View.VISIBLE);

                viewHolder.trash.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        deleteReview(mArrayList.get(i).getIdReview(), mArrayList.get(i).getIdWisata());
                    }
                });
            }
            else{
                viewHolder.trash.setVisibility(View.GONE);
            }

        }

        @Override
        public int getItemCount() {
            return mArrayList.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            TextView txtUsername;
           // TextView txtTanggal;
            TextView txtUlasan;
            ImageView trash;
            CardView cvPopular;

            public ViewHolder(View itemView) {
                super(itemView);
                txtUsername= (TextView) itemView.findViewById(R.id.txtUsername);
                txtUlasan = (TextView) itemView.findViewById(R.id.txtUlasan);
                trash = (ImageView) itemView.findViewById(R.id.trash);
                cvPopular = (CardView) itemView.findViewById(R.id.cvPopular);
            }
        }

    }

    public void deleteReview(final int idReview, final  int idWisata){
        requestQueue = Volley.newRequestQueue(this);
        String urls = this.getString(R.string.api) + "deleteReview.php";
        stringRequest = new StringRequest(Request.Method.POST, urls, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonResponse = new JSONObject(response);
                    boolean success = jsonResponse.getBoolean("success");
                    JSONObject j = null;

                    if (success) {
                        Toast.makeText(DetailActivity.this, "Success!", Toast.LENGTH_SHORT).show();
                        listReview.clear();
                        getReview(idWisata);
                    } else {
                        Toast.makeText(DetailActivity.this, "Failed!", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(DetailActivity.this, "Error", Toast.LENGTH_SHORT).show();
            }
        }){
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("idReview", Integer.toString(idReview));
                return params;
            }
        };
        requestQueue.add(stringRequest);
    }
}
