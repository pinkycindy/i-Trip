package com.ppl.fikkrip.itrip.controller.activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.ppl.fikkrip.itrip.R;
import com.ppl.fikkrip.itrip.model.DetailPlanning;
import com.ppl.fikkrip.itrip.sharedpreference.SessionManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class DetailPlanningActivity extends AppCompatActivity {

    private ImageButton buttonDone;
    private TextView tvTitle, tvTanggal;
    private ListDetailPlanningAdapter adapter;
    private ArrayList<DetailPlanning> listDetailPlanning = null;
    private RecyclerView recyclerView;
    private RequestQueue requestQueue;
    private StringRequest stringRequest;
    private ImageButton ibTambah;
    private SessionManager session;
    private FloatingActionButton fab;
    private String idPlanning, idUser;

    @Override
    protected void onResume() {
        listDetailPlanning.clear();
        getListDetailPlanning();
        super.onResume();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_planning);

        listDetailPlanning = new ArrayList<>();

        buttonDone = (ImageButton) findViewById(R.id.done_button);
        tvTitle = (TextView) findViewById(R.id.toolbar_title);
        tvTanggal = (TextView) findViewById(R.id.tv_tanggal);
        recyclerView = (RecyclerView) findViewById(R.id.list_wisata);
        ibTambah = (ImageButton) findViewById(R.id.ib_tambah);

        tvTitle.setText(getIntent().getStringExtra("judul"));
        tvTanggal.setText(getIntent().getStringExtra("tglPlanning"));
        idPlanning = getIntent().getStringExtra("idPlanning");

        buttonDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        ibTambah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DetailPlanningActivity.this, SearchActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString("idPlanning", idPlanning);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });

        if (listDetailPlanning == null) {
            getListDetailPlanning();
        }

        LinearLayoutManager llm = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(llm);
    }

    public void getListDetailPlanning() {
        String url = getString(R.string.api) + "GetDetailPlanning.php";
        requestQueue = Volley.newRequestQueue(DetailPlanningActivity.this);
        stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    JSONArray jsonArray = jsonObject.getJSONArray("getDetail");
                    for (int a = 0; a < jsonArray.length(); a++) {
                        JSONObject json = jsonArray.getJSONObject(a);
                        DetailPlanning detailPlanning = new DetailPlanning();
                        detailPlanning.setIdWisata(json.getInt("idWisata"));
                        detailPlanning.setIdDetail(json.getInt("idDetail"));
                        detailPlanning.setIdPlanning(json.getInt("idPlanning"));
                        detailPlanning.setNamaWisata(json.getString("namaWisata"));
                        detailPlanning.setLokasi(json.getString("lokasiWisata"));
                        detailPlanning.setGambar(json.getString("gambarWisata"));
                        listDetailPlanning.add(a, detailPlanning);
                        adapter = new ListDetailPlanningAdapter(DetailPlanningActivity.this, listDetailPlanning);
                        recyclerView.setAdapter(adapter);
                    }
                } catch (JSONException e) {
                    Toast.makeText(DetailPlanningActivity.this, "Data tidak Ada", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(DetailPlanningActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("idPlanning", idPlanning);
                return params;
            }
        };
        requestQueue.add(stringRequest);
    }

    public class ListDetailPlanningAdapter extends RecyclerView.Adapter<ListDetailPlanningAdapter.ViewHolder> {

        Context c;
        private RequestQueue requestQueue;
        private StringRequest stringRequest;

        public ListDetailPlanningAdapter(Context con, ArrayList<DetailPlanning> arrayList) {
            this.c = con;
            listDetailPlanning = arrayList;
        }

        @Override
        public ListDetailPlanningAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_detail_planning, null);
            return new ListDetailPlanningAdapter.ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(final ViewHolder viewHolder, final int i) {
            Glide.with(c)
                    .load(c.getString(R.string.img) + listDetailPlanning.get(i).getGambar()).crossFade()
                    .placeholder(R.drawable.ic_nature)
                    .into(viewHolder.imghape);
            viewHolder.txtWisata.setText(listDetailPlanning.get(i).getNamaWisata());
            viewHolder.txtLokasi.setText(listDetailPlanning.get(i).getLokasi());

            viewHolder.cvDetailPlanning.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(c);
                    builder.setTitle("Delete Planning");
                    builder.setMessage("Are you sure?").setCancelable(false)
                            .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    DeleteWisata(Integer.toString(listDetailPlanning.get(i).getIdDetail()));
                                    Toast.makeText(c, listDetailPlanning.get(i).getNamaWisata() + " is deleted", Toast.LENGTH_SHORT).show();
                                }
                            })
                            .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.cancel();
                                }
                            });

                    AlertDialog alertDialog = builder.create();
                    alertDialog.show();
                    return false;
                }
            });

//            viewHolder.cvPopular.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    Intent intent = new Intent(c, DetailPlanningActivity.class);
//                    Bundle bundle = new Bundle();
//                    bundle.putString("idPlanning", Integer.toString(mArrayList.get(i).getIdPlanning()));
//                    bundle.putString("judul", mArrayList.get(i).getJudul());
//                    bundle.putString("tglPlanning", mArrayList.get(i).getTglPlanning());
//                    intent.putExtras(bundle);
//                    c.startActivity(intent);
//                }
//            });
        }

        @Override
        public int getItemCount() {
            return listDetailPlanning.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            TextView txtWisata;
            TextView txtLokasi;
            ImageView imghape;
            CardView cvDetailPlanning;

            public ViewHolder(View itemView) {
                super(itemView);
                txtWisata = (TextView) itemView.findViewById(R.id.txt_wisata);
                txtLokasi = (TextView) itemView.findViewById(R.id.txt_lokasi);
                imghape = (ImageView) itemView.findViewById(R.id.imghp);
                cvDetailPlanning = (CardView) itemView.findViewById(R.id.cv_detail_planning);
            }
        }

        public void DeleteWisata(final String idDetail) {
            requestQueue = Volley.newRequestQueue(c);
            String urls = c.getString(R.string.api) + "DeleteWisata.php";
            stringRequest = new StringRequest(Request.Method.POST, urls, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    try {
                        JSONObject jsonResponse = new JSONObject(response);
                        boolean success = jsonResponse.getBoolean("success");
                        JSONObject j = null;

                        if (success) {
                            listDetailPlanning.clear();
                            adapter.notifyDataSetChanged();
                            getListDetailPlanning();
                        } else {
                            Toast.makeText(c, "Failed!", Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(c, "Error", Toast.LENGTH_SHORT).show();
                }
            }){
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<>();
                    params.put("idDetail", idDetail);
                    return params;
                }
            };
            requestQueue.add(stringRequest);
        }
    }
}
