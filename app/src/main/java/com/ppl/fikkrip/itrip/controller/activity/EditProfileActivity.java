package com.ppl.fikkrip.itrip.controller.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.Volley;
import com.ppl.fikkrip.itrip.R;
import com.ppl.fikkrip.itrip.rest.EditProfileRequest;
import com.ppl.fikkrip.itrip.sharedpreference.SessionManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class EditProfileActivity extends AppCompatActivity {

    SessionManager session;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);

        session = new SessionManager(this);
        session.checkLogin();
        HashMap<String, String> user = session.getUserDetails();

        final String idUser = user.get(SessionManager.KEY_ID);
        String nama = user.get(SessionManager.KEY_NAMA);
        final String username = user.get(SessionManager.KEY_USERNAME);
        String email = user.get(SessionManager.KEY_EMAIL);

        Button btnSave = (Button) findViewById(R.id.btn_save);
        Button btnCancel = (Button) findViewById(R.id.btn_cancel);
        final EditText etNama = (EditText) findViewById(R.id.nama);
        final EditText etEmail = (EditText) findViewById(R.id.email);
        final EditText etPassword = (EditText) findViewById(R.id.password);
        final EditText etRePassword = (EditText) findViewById(R.id.rePassword);


        // Display user details
        etNama.setText(nama);
        etEmail.setText(email);

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String nama = etNama.getText().toString();
                String email = etEmail.getText().toString();
                String password = etPassword.getText().toString();
                String rePassword = etRePassword.getText().toString();

                if (nama.length() == 0) {
                    //jika form Email belum di isi / masih kosong
                    etNama.setError("Please Enter your Name!");
                    etNama.requestFocus();
                } else if ((email.length() == 0) || !(Patterns.EMAIL_ADDRESS.matcher(email).matches())) {
                    //jika form Email belum di isi / penulisan email salah
                    etEmail.setError("Please Enter your Valid Email!");
                    etEmail.requestFocus();
                } else if (!rePassword.equals(password)) {
                    //jika form RePassword tidak sama dengan Password
                    etRePassword.requestFocus();
                    Toast.makeText(EditProfileActivity.this, "Please Enter same Password!", Toast.LENGTH_SHORT).show();}
                else{
                    progressDialog = new ProgressDialog(EditProfileActivity.this);
                    progressDialog.setTitle("Please Wait");
                    progressDialog.show();
                    progressDialog.setMessage("Updating...");

                    Response.Listener<String> responseListener = new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                JSONObject jsonResponse = new JSONObject(response);
                                boolean success = jsonResponse.getBoolean("success");
                                if (success) {
                                    progressDialog.dismiss();
                                    Toast.makeText(getApplicationContext(), "Update Profile Success!", Toast.LENGTH_SHORT).show();
                                    String idUser = jsonResponse.getString("idUser");
                                    String nama = etNama.getText().toString();
                                    String email = etEmail.getText().toString();
                                    String password = etPassword.getText().toString();
                                    Intent intent = new Intent(EditProfileActivity.this, ProfileActivity.class);
                                    startActivity(intent);
                                    session.createLoginSession(idUser, nama, username, email);
                                    finish();
                                } else {
                                    progressDialog.dismiss();
                                    AlertDialog.Builder builder = new AlertDialog.Builder(EditProfileActivity.this);
                                    builder.setMessage("Username is Available, change the username!")
                                            .setNegativeButton("Retry", null)
                                            .create()
                                            .show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    };

                    EditProfileRequest editProfileRequest = new EditProfileRequest(idUser, nama, username, password, email, getString(R.string.api)+"EditProfile.php", responseListener);
                    RequestQueue queue = Volley.newRequestQueue(EditProfileActivity.this);
                    queue.add(editProfileRequest);
                }
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(EditProfileActivity.this, ProfileActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }

    @Override
    public void onBackPressed()
    {
        Intent intent = new Intent(EditProfileActivity.this, ProfileActivity.class);
        startActivity(intent);
        finish();
    }
}
