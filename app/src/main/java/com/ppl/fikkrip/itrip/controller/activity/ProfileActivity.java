package com.ppl.fikkrip.itrip.controller.activity;

import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.ppl.fikkrip.itrip.R;
import com.ppl.fikkrip.itrip.sharedpreference.SessionManager;

import java.util.HashMap;

public class ProfileActivity extends AppCompatActivity {

    SessionManager session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        ImageButton editProfile = (ImageButton) findViewById(R.id.edit_button);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        final Drawable upArrow = ContextCompat.getDrawable(this, R.drawable.ic_arrow_back);
        upArrow.setColorFilter(ContextCompat.getColor(this, R.color.colorWhite), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);

        session = new SessionManager(this);
        session.checkLogin();
        HashMap<String, String> user = session.getUserDetails();

        String nama = user.get(SessionManager.KEY_NAMA);
        String username = user.get(SessionManager.KEY_USERNAME);
        String email = user.get(SessionManager.KEY_EMAIL);

        TextView etNama = (TextView) findViewById(R.id.nama);
        TextView etUsername = (TextView) findViewById(R.id.username);
        TextView etEmail = (TextView) findViewById(R.id.email);

        // Display user details
        etNama.setText(nama);
        etNama.setEnabled(false);
        etUsername.setText(username);
        etUsername.setEnabled(false);
        etEmail.setText(email);
        etEmail.setEnabled(false);

        editProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ProfileActivity.this, EditProfileActivity.class));
                finish();
            }
        });
    }
}
