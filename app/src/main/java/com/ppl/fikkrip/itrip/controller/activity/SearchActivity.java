package com.ppl.fikkrip.itrip.controller.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.ppl.fikkrip.itrip.R;
import com.ppl.fikkrip.itrip.model.DataWisata;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class SearchActivity extends AppCompatActivity {

    private RecyclerView lvhape;
    private RequestQueue requestQueue;
    private StringRequest stringRequest;
    private ArrayList<DataWisata> mArrayList;
    private ArrayList<DataWisata> mFilteredList;
    private ListSearchWisataAdapter mAdapter;
    private String idPlanning, idWisata;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        idPlanning = getIntent().getStringExtra("idPlanning");

        initViews();
        loadJSON();
    }

    private void initViews(){
        lvhape = (RecyclerView) findViewById(R.id.lvhape);
        lvhape.setHasFixedSize(true);
        LinearLayoutManager llm = new LinearLayoutManager(this);
        lvhape.setLayoutManager(llm);
    }

    private void loadJSON(){
        String url = getString(R.string.api)+"getAllWisata.php";
        requestQueue = Volley.newRequestQueue(this);
        mArrayList = new ArrayList<>();
        mFilteredList = new ArrayList<>();

        stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    JSONArray jsonArray = jsonObject.getJSONArray("getAllWisata");
                    for (int a = 0; a < jsonArray.length(); a++) {
                        JSONObject json = jsonArray.getJSONObject(a);
                        DataWisata save = new DataWisata();
                        save.setIdWisata(json.getInt("idWisata"));
                        save.setNama(json.getString("namaWisata"));
                        save.setLokasi(json.getString("lokasiWisata"));
                        save.setGambar(json.getString("gambarWisata"));
                        mArrayList.add(a, save);
                        mAdapter = new ListSearchWisataAdapter(SearchActivity.this, mArrayList);
                        lvhape.setAdapter(mAdapter);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(SearchActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

        requestQueue.add(stringRequest);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);

        MenuItem search = menu.findItem(R.id.search);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(search);
        search(searchView);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    private void search(SearchView searchView) {
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                mAdapter.getFilter().filter(newText);
                return true;
            }
        });
    }

    public class ListSearchWisataAdapter extends RecyclerView.Adapter<ListSearchWisataAdapter.ViewHolder> implements Filterable {

        Context c;

        public ListSearchWisataAdapter(Context con, ArrayList<DataWisata> arrayList) {
            this.c = con;
            mArrayList = arrayList;
            mFilteredList = arrayList;
        }

        @Override
        public ListSearchWisataAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_detail_planning, null);
            return new ListSearchWisataAdapter.ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(final ViewHolder viewHolder, final int i) {
            viewHolder.txtWisata.setText(mFilteredList.get(i).getNama());
            viewHolder.txtLokasi.setText(mFilteredList.get(i).getLokasi());
            Glide.with(c)
                    .load(c.getString(R.string.img) + mFilteredList.get(i).getGambar()).into(viewHolder.imghape);

            viewHolder.cvDetailPlanning.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    idWisata = Integer.toString(mFilteredList.get(i).getIdWisata());
                    saveDetailPlanning();
                }
            });
        }

        @Override
        public Filter getFilter() {

            return new Filter() {
                @Override
                protected FilterResults performFiltering(CharSequence charSequence) {

                    String charString = charSequence.toString();

                    if (charString.isEmpty()) {

                        mFilteredList = mArrayList;
                    } else {

                        ArrayList<DataWisata> filteredList = new ArrayList<>();

                        for (DataWisata androidVersion : mArrayList) {

                            if (androidVersion.getNama().toLowerCase().contains(charString) || androidVersion.getLokasi().toLowerCase().contains(charString) || androidVersion.getGambar().toLowerCase().contains(charString)) {
                                filteredList.add(androidVersion);
                            }
                        }

                        mFilteredList = filteredList;
                    }

                    FilterResults filterResults = new FilterResults();
                    filterResults.values = mFilteredList;
                    return filterResults;
                }

                @Override
                protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                    mFilteredList = (ArrayList<DataWisata>) filterResults.values;
                    notifyDataSetChanged();
                }
            };
        }

        @Override
        public int getItemCount() {
            return mFilteredList.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            TextView txtWisata;
            TextView txtLokasi;
            ImageView imghape;
            CardView cvDetailPlanning;

            public ViewHolder(View itemView) {
                super(itemView);
                txtWisata = (TextView) itemView.findViewById(R.id.txt_wisata);
                txtLokasi = (TextView) itemView.findViewById(R.id.txt_lokasi);
                imghape = (ImageView) itemView.findViewById(R.id.imghp);
                cvDetailPlanning = (CardView) itemView.findViewById(R.id.cv_detail_planning);
            }
        }

        void saveDetailPlanning(){
            RequestQueue requestQueue1;
            StringRequest stringRequest1;
            final ProgressDialog progressDialog = new ProgressDialog(SearchActivity.this);

            progressDialog.setTitle("Please Wait");
            progressDialog.show();
            progressDialog.setMessage("Processing...");

            String url = getString(R.string.api)+"SaveDetailPlanning.php";
            requestQueue1 = Volley.newRequestQueue(SearchActivity.this);
            stringRequest1 = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    try {
                        JSONObject jsonResponse = new JSONObject(response);
                        boolean success = jsonResponse.getBoolean("success");

                        if (success) {
                            progressDialog.dismiss();
                            Toast.makeText(getApplicationContext(), "Success!", Toast.LENGTH_SHORT).show();
                            finish();
                        } else {
                            progressDialog.dismiss();
                            AlertDialog.Builder builder = new AlertDialog.Builder(SearchActivity.this);
                            builder.setMessage("Error!!!")
                                    .setNegativeButton("Retry", null)
                                    .create()
                                    .show();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(SearchActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }){
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<>();
                    params.put("idPlanning", idPlanning);
                    params.put("idWisata", idWisata);
                    return params;
                }
            };
            requestQueue1.add(stringRequest1);
        }
    }
}
