package com.ppl.fikkrip.itrip.controller.activity;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.ppl.fikkrip.itrip.R;
import com.ppl.fikkrip.itrip.model.FavoritModel;
import com.ppl.fikkrip.itrip.rest.FavoritRequest;
import com.ppl.fikkrip.itrip.rest.ProvinsiRequest;
import com.ppl.fikkrip.itrip.sharedpreference.SessionManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Pinky Cindy on 10/25/17.
 */

public class WisataActivity extends AppCompatActivity {

    private ListWisataAdapter adapter;
    private Spinner spinner;
    private String idPulau, idKategori, pilih;
    private ArrayList<String> provinsiList;
    private JSONArray result;
    private RecyclerView lvhape;
    private ArrayList<FavoritModel> list_data = null;
    private TextView txtTittle;
    private ImageButton btnBack;
    SessionManager session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wisata);

        list_data  = new ArrayList<>();
        Toolbar ToolBarAtas = (Toolbar) findViewById(R.id.toolbar);
        spinner = (Spinner) findViewById(R.id.spinner);
        idPulau = getIntent().getStringExtra("idPulau");
        idKategori = getIntent().getStringExtra("idKategori");
        txtTittle  = (TextView) findViewById(R.id.toolbar_title);
        btnBack = (ImageButton) findViewById(R.id.toolbar_back);
        provinsiList = new ArrayList<String>();

        if(idKategori.equals("alam")){
            txtTittle.setText("NATURE");
        }
        else if(idKategori.equals("modern")){
            txtTittle.setText("MODERN");
        }
        else if(idKategori.equals("budaya")){
            txtTittle.setText("CULTURE");
        }
        else if(idKategori.equals("kuliner")){
            txtTittle.setText("CULINARY");
        }

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                overridePendingTransition(R.anim.left_to_right, R.anim.left_to_right2);
            }
        });
        spinner.setOnItemSelectedListener(new ItemSelectedListener());

        getData(idPulau);

        lvhape = (RecyclerView) findViewById(R.id.listwisata);
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(this, 2);
        lvhape.setLayoutManager(mLayoutManager);
        lvhape.addItemDecoration(new GridSpacingItemDecoration(2, dpToPx(10), true));
        lvhape.setItemAnimator(new DefaultItemAnimator());
    }

    private void getData(String idPulau) {
        String url = getString(R.string.api) + "getProvinsi.php";
        Response.Listener<String> responseListener2 = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                JSONObject j = null;
                try {
                    j = new JSONObject(response);

                    result = j.getJSONArray("getProvinsi");

                    getProvinsi(result);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        };
        ProvinsiRequest provinsiRequest = new ProvinsiRequest(idPulau, url, responseListener2);
        RequestQueue queue = Volley.newRequestQueue(WisataActivity.this);
        queue.add(provinsiRequest);
    }

    private void getProvinsi(JSONArray j) {
        //Traversing through all the items in the json array
        for (int i = 0; i < j.length(); i++) {
            try {
                //Getting json object
                JSONObject json = j.getJSONObject(i);

                //Adding the name of the student to array list
                provinsiList.add(json.getString("namaProvinsi"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        //Setting adapter to show the items in the spinner
        spinner.setAdapter(new ArrayAdapter<String>(WisataActivity.this, android.R.layout.simple_spinner_dropdown_item, provinsiList));
    }


    public class ItemSelectedListener implements AdapterView.OnItemSelectedListener {

        //get strings of first item
        String firstItem = String.valueOf(spinner.getSelectedItem());

        public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
            if (firstItem.equals(String.valueOf(spinner.getSelectedItem()))) {

            } else {
                list_data.clear();
                pilih = parent.getItemAtPosition(pos).toString();
                getDataWisata(pilih, idKategori);
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> arg) {

        }
    }

    private void getDataWisata(String idProvinsi, String idKategori) {
        session = new SessionManager(getApplicationContext());
        session.checkLogin();
        HashMap<String, String> user = session.getUserDetails();
        String idUser = user.get(SessionManager.KEY_ID);

        String url = getString(R.string.api) + "getDataWisata.php";
        Response.Listener<String> responseListener = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                JSONObject j = null;
                try {
                    j = new JSONObject(response);
                    result = j.getJSONArray("getWisata");
                    getWisata(result);
                } catch (JSONException e) {
                    e.printStackTrace();
                    adapter.notifyDataSetChanged();
                }
            }
        };

        ProvinsiRequest provinsiRequest = new ProvinsiRequest(idProvinsi, idKategori, idUser, url, responseListener);
        RequestQueue queue = Volley.newRequestQueue(WisataActivity.this);
        queue.add(provinsiRequest);
    }

    private void getWisata(JSONArray j) {
        //Traversing through all the items in the json array
        for (int i = 0; i < j.length(); i++) {
            try {
                //Getting json object
                JSONObject json = result.getJSONObject(i);
                FavoritModel favoritModel = new FavoritModel();
                favoritModel.setIdWisata(json.getInt("idWisata"));
                favoritModel.setIdProvinsi(json.getInt("idProvinsi"));
                favoritModel.setNamaProvinsi(json.getString("namaProvinsi"));
                favoritModel.setNamaWisata(json.getString("namaWisata"));
                favoritModel.setDeskripsiWisata(json.getString("deskripsiWisata"));
                favoritModel.setKategori(json.getString("kategori"));
                favoritModel.setBiayaMasuk(json.getInt("biayaMasuk"));
                favoritModel.setLongtitude(json.getDouble("longtitude"));
                favoritModel.setLatitude(json.getDouble("latitude"));
                favoritModel.setLokasi(json.getString("lokasiWisata"));
                favoritModel.setGambar(json.getString("gambarWisata"));
                favoritModel.setStatus(json.getString("status"));
                favoritModel.setIdUser(json.getString("idUser"));
                list_data.add(favoritModel);
                adapter = new ListWisataAdapter(this,list_data);
                lvhape.setAdapter(adapter);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        if (adapter == null ) {
            adapter = new ListWisataAdapter(this, list_data);
            lvhape.setAdapter(adapter);
        }else {
            adapter.notifyDataSetChanged();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    /**
     * RecyclerView item decoration - give equal margin around grid item
     */
    public class GridSpacingItemDecoration extends RecyclerView.ItemDecoration {

        private int spanCount;
        private int spacing;
        private boolean includeEdge;

        GridSpacingItemDecoration(int spanCount, int spacing, boolean includeEdge) {
            this.spanCount = spanCount;
            this.spacing = spacing;
            this.includeEdge = includeEdge;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            int position = parent.getChildAdapterPosition(view); // item position
            int column = position % spanCount; // item column

            if (includeEdge) {
                outRect.left = spacing - column * spacing / spanCount; // spacing - column * ((1f / spanCount) * spacing)
                outRect.right = (column + 1) * spacing / spanCount; // (column + 1) * ((1f / spanCount) * spacing)

                if (position < spanCount) { // top edge
                    outRect.top = spacing;
                }
                outRect.bottom = spacing; // item bottom
            } else {
                outRect.left = column * spacing / spanCount; // column * ((1f / spanCount) * spacing)
                outRect.right = spacing - (column + 1) * spacing / spanCount; // spacing - (column + 1) * ((1f /    spanCount) * spacing)
                if (position >= spanCount) {
                    outRect.top = spacing; // item top
                }
            }
        }
    }

    /**
     * Converting dp to pixel
     */
    private int dpToPx(int dp) {
        Resources r = getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }

    public class ListWisataAdapter extends RecyclerView.Adapter<ListWisataAdapter.ViewHolder>{

        Context c;
        ArrayList<FavoritModel>list_data;

        public ListWisataAdapter(Context con, ArrayList<FavoritModel> list_data) {
            this.c = con;
            this.list_data = list_data;
        }

        @Override
        public ListWisataAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_wisata, null);
            return new ListWisataAdapter.ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, final int position) {
            Glide.with(c)
                    .load(c.getString(R.string.img)+list_data.get(position).getGambar()).fitCenter()
                    .placeholder(R.drawable.ic_nature)
                    .into(holder.imghape);

            holder.txtWisata.setText(list_data.get(position).getNamaWisata());
            holder.txtLokasi.setText(list_data.get(position).getLokasi());
            if(list_data.get(position).getStatus().equals("")){
                holder.favorit.setImageResource(R.drawable.ic_star_2);
                holder.favorit.setOnClickListener(new View.OnClickListener(){
                    @Override
                    public void onClick(View v) {
                        SessionManager session = new SessionManager(c);
                        session.checkLogin();
                        HashMap<String, String> user = session.getUserDetails();
                        String idUser = user.get(SessionManager.KEY_ID);
                        saveFavorit(list_data.get(position).getIdWisata(), idUser);
                    }
                });
            }
            else{
                holder.favorit.setImageResource(R.drawable.ic_star_1);
                holder.favorit.setOnClickListener(new View.OnClickListener(){
                    @Override
                    public void onClick(View v) {
                        SessionManager session = new SessionManager(c);
                        session.checkLogin();
                        HashMap<String, String> user = session.getUserDetails();
                        String idUser = user.get(SessionManager.KEY_ID);
                        deleteFavorit(list_data.get(position).getIdWisata(), idUser);
                    }
                });
            }

            holder.imghape.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(c, DetailActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putInt("idWisata", list_data.get(position).getIdWisata());
                    bundle.putString("namaWisata", list_data.get(position).getNamaWisata());
                    bundle.putInt("biayaMasuk", list_data.get(position).getBiayaMasuk());
                    bundle.putString("lokasiWisata", list_data.get(position).getLokasi());
                    bundle.putString("deskripsiWisata", list_data.get(position).getDeskripsiWisata());
                    bundle.putString("gambarWisata", list_data.get(position).getGambar());
                    bundle.putDouble("longtitude", list_data.get(position).getLongtitude());
                    bundle.putDouble("latitude", list_data.get(position).getLatitude());
                    intent.putExtras(bundle);
                    c.startActivity(intent);
                }
            });

            holder.cvPopular.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent intent = new Intent(c, DetailActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putInt("idWisata", list_data.get(position).getIdWisata());
                    bundle.putString("namaWisata", list_data.get(position).getNamaWisata());
                    bundle.putInt("biayaMasuk", list_data.get(position).getBiayaMasuk());
                    bundle.putString("lokasiWisata", list_data.get(position).getLokasi());
                    bundle.putString("deskripsiWisata", list_data.get(position).getDeskripsiWisata());
                    bundle.putString("gambarWisata", list_data.get(position).getGambar());
                    bundle.putDouble("longtitude", list_data.get(position).getLongtitude());
                    bundle.putDouble("latitude", list_data.get(position).getLatitude());
                    intent.putExtras(bundle);
                    c.startActivity(intent);
                }
            });

            holder.btnShare.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent sharingIntent = new Intent(Intent.ACTION_SEND);
                    sharingIntent.setType("text/plain");
                    sharingIntent.putExtra(Intent.EXTRA_SUBJECT, "I-Trip");
                    sharingIntent.putExtra(Intent.EXTRA_TEXT, "Wisata :"+list_data.get(position).getNamaWisata()+" Lokasi : "+list_data.get(position).getLokasi());
                    sharingIntent.putExtra(Intent.EXTRA_TEXT, "Wisata : "+list_data.get(position).getNamaWisata()+" Lokasi : "+list_data.get(position).getLokasi()+" (https://www.google.com/maps/place/"+list_data.get(position).getLongtitude()+","+list_data.get(position).getLatitude()+")");
                    c.startActivity(Intent.createChooser(sharingIntent, "Share via"));
                }
            });
        }

        @Override
        public int getItemCount() {
            return list_data.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            TextView txtWisata;
            TextView txtLokasi;
            ImageView imghape, favorit;
            CardView cvPopular;
            ImageView btnShare;

            public ViewHolder(View itemView) {
                super(itemView);
                txtWisata = (TextView) itemView.findViewById(R.id.txt_nama);
                txtLokasi = (TextView) itemView.findViewById(R.id.txt_lokasi);
                imghape = (ImageView) itemView.findViewById(R.id.imghp);
                cvPopular = (CardView) itemView.findViewById(R.id.card_view);
                favorit = (ImageView) itemView.findViewById(R.id.btn_favorit);
                btnShare = (ImageView) itemView.findViewById(R.id.btn_share);
            }
        }

        public void saveFavorit(final int idWisata, final String idUser){
            String url = c.getString(R.string.api) + "saveFavorit.php";
            Response.Listener<String> responseListener = new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    try {
                        JSONObject jsonResponse = new JSONObject(response);
                        boolean success = jsonResponse.getBoolean("success");
                        JSONObject j = null;

                        if (success) {
                            Toast.makeText(c, "Save!", Toast.LENGTH_SHORT).show();
                            list_data.clear();
                            adapter.notifyDataSetChanged();
                            getDataWisata(pilih, idKategori);
                        } else {
                            Toast.makeText(c, "Failed!", Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            };
            String idW = Integer.toString(idWisata);

            FavoritRequest addRequest = new FavoritRequest(idW, idUser, url, responseListener);
            RequestQueue queue = Volley.newRequestQueue(c);
            queue.add(addRequest);
        }
        public void deleteFavorit(int idWisata, final String idUser){
            String url = c.getString(R.string.api) + "deleteFavorit.php";
            Response.Listener<String> responseListener = new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    try {

                        JSONObject jsonResponse = new JSONObject(response);
                        boolean success = jsonResponse.getBoolean("success");
                        JSONObject j = null;

                        if (success) {
                            Toast.makeText(c, "Success!", Toast.LENGTH_SHORT).show();
                            list_data.clear();
                            adapter.notifyDataSetChanged();
                            getDataWisata(pilih, idKategori);
                        } else {
                            Toast.makeText(c, "Failed!", Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            };
            String idW = Integer.toString(idWisata);

            FavoritRequest addRequest = new FavoritRequest(idW, idUser, url, responseListener);
            RequestQueue queue = Volley.newRequestQueue(c);
            queue.add(addRequest);
        }
    }
}
