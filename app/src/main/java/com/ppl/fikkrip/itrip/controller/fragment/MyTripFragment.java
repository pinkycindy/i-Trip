package com.ppl.fikkrip.itrip.controller.fragment;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.DefaultSliderView;
import com.ppl.fikkrip.itrip.R;
import com.ppl.fikkrip.itrip.controller.activity.AddPlanningActivity;
import com.ppl.fikkrip.itrip.controller.activity.DetailPlanningActivity;
import com.ppl.fikkrip.itrip.model.PlanningModel;
import com.ppl.fikkrip.itrip.sharedpreference.SessionManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class MyTripFragment extends Fragment {
    private ListPlanningAdapter adapter;
    private ArrayList<PlanningModel> listPlanning = null;
    private RecyclerView lvhape;
    private RequestQueue requestQueue;
    private StringRequest stringRequest;
    private SliderLayout sliderLayout;
    private SessionManager session;
    private FloatingActionButton fab;
    private String idUser;
    private ImageView img;
    private TextView tError;

    @Override
    public void onResume() {
        listPlanning.clear();
        getListPlanning();
        super.onResume();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        listPlanning = new ArrayList<>();

        session = new SessionManager(getContext());
        session.checkLogin();
        HashMap<String, String> user = session.getUserDetails();
        idUser = user.get(SessionManager.KEY_ID);

        if (listPlanning == null){
            getListPlanning();
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_my_trip, container, false);

        sliderLayout = (SliderLayout) view.findViewById(R.id.slider);
        lvhape = (RecyclerView) view.findViewById(R.id.lvhape);
        fab = (FloatingActionButton) view.findViewById(R.id.fab);
        img = (ImageView) view.findViewById(R.id.gEvent);
        tError = (TextView) view.findViewById(R.id.tEror);

        LinearLayoutManager llm = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        lvhape.setLayoutManager(llm);
        adapter = new ListPlanningAdapter(getContext(), listPlanning);
        lvhape.setAdapter(adapter);

        HashMap<String,Integer> file_maps = new HashMap<>();
        file_maps.put("Promo 1", R.drawable.promo1);
        file_maps.put("Promo 2", R.drawable.promo2);
        file_maps.put("Promo 3", R.drawable.promo3);
        file_maps.put("Promo 4", R.drawable.promo4);
        file_maps.put("Promo 5", R.drawable.promo5);

        for(final String name : file_maps.keySet()){
            DefaultSliderView defaultSliderView = new DefaultSliderView(getActivity());
            // initialize a SliderLayout
            defaultSliderView
                    .image(file_maps.get(name))
                    .setScaleType(BaseSliderView.ScaleType.Fit)
                    .setOnSliderClickListener(new BaseSliderView.OnSliderClickListener() {
                        @Override
                        public void onSliderClick(BaseSliderView slider) {
                            if(name.equals("Promo 1")){
                                Intent intent = new Intent();
                                intent.setAction(Intent.ACTION_VIEW);
                                intent.addCategory(Intent.CATEGORY_BROWSABLE);
                                intent.setData(Uri.parse("https://www.traveloka.com/promotion/cceoy"));
                                startActivity(intent);
                            } else if(name.equals("Promo 2")){
                                Intent intent = new Intent();
                                intent.setAction(Intent.ACTION_VIEW);
                                intent.addCategory(Intent.CATEGORY_BROWSABLE);
                                intent.setData(Uri.parse("https://www.traveloka.com/promotion/everyday"));
                                startActivity(intent);
                            } else if(name.equals("Promo 3")){
                                Intent intent = new Intent();
                                intent.setAction(Intent.ACTION_VIEW);
                                intent.addCategory(Intent.CATEGORY_BROWSABLE);
                                intent.setData(Uri.parse("https://www.traveloka.com/promotion/tgif"));
                                startActivity(intent);
                            } else if(name.equals("Promo 4")){
                                Intent intent = new Intent();
                                intent.setAction(Intent.ACTION_VIEW);
                                intent.addCategory(Intent.CATEGORY_BROWSABLE);
                                intent.setData(Uri.parse("https://www.traveloka.com/promotion/mandiri"));
                                startActivity(intent);
                            } else if(name.equals("Promo 5")) {
                                Intent intent = new Intent();
                                intent.setAction(Intent.ACTION_VIEW);
                                intent.addCategory(Intent.CATEGORY_BROWSABLE);
                                intent.setData(Uri.parse("https://www.traveloka.com/promotion/jogja"));
                                startActivity(intent);
                            } else{
                                Toast.makeText(getContext(), "Gagal", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
            //add your extra information
            defaultSliderView.bundle(new Bundle());
            defaultSliderView.getBundle().putString("extra",name);
            sliderLayout.addSlider(defaultSliderView);
        }
        sliderLayout.setPresetTransformer(SliderLayout.Transformer.Accordion);
        sliderLayout.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
        sliderLayout.setDuration(6000);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getContext(), AddPlanningActivity.class));
            }
        });

        return view;
    }

    public void getListPlanning() {
        String url = getString(R.string.api) + "listPlanning.php";

        requestQueue = Volley.newRequestQueue(getActivity());
        stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    JSONArray jsonArray = jsonObject.getJSONArray("listplanning");
                    for (int a = 0; a < jsonArray.length(); a++) {
                        JSONObject json = jsonArray.getJSONObject(a);
                        PlanningModel planningModel = new PlanningModel();
                        planningModel.setIdPlanning(json.getInt("idPlanning"));
                        planningModel.setCount(json.getInt("jumlah"));
                        planningModel.setJudul(json.getString("judul"));
                        planningModel.setTglPlanning(json.getString("tglPlanning"));
                        listPlanning.add(planningModel);
                    }
                    if(listPlanning.size()<1){
                        img.setVisibility(View.VISIBLE);
                        tError.setVisibility(View.VISIBLE);
                    }
                    else{
                        img.setVisibility(View.GONE);
                        tError.setVisibility(View.GONE);
                    }
                    adapter.notifyDataSetChanged();


                } catch (JSONException e) {
                    Toast.makeText(getActivity(), "Data tidak Ada", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getActivity(), error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("idUser", idUser);
                return params;
            }
        };
        requestQueue.add(stringRequest);
    }

    public class ListPlanningAdapter extends RecyclerView.Adapter<ListPlanningAdapter.ViewHolder> {

        Context c;
        private ArrayList<PlanningModel> mArrayList;
        private RequestQueue requestQueue;
        private StringRequest stringRequest;

        public ListPlanningAdapter(Context con, ArrayList<PlanningModel> arrayList) {
            this.c = con;
            mArrayList = arrayList;
        }

        @Override
        public ListPlanningAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_planning, null);
            return new ListPlanningAdapter.ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(final ListPlanningAdapter.ViewHolder viewHolder, final int i) {

            viewHolder.txtWisata.setText(mArrayList.get(i).getJudul());
            viewHolder.txtTanggal.setText(" " +mArrayList.get(i).getTglPlanning() + " | " + mArrayList.get(i).getCount() + " destinations");

            viewHolder.cvPopular.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(c);
                    builder.setTitle("Delete Planning");
                    builder.setMessage("Are you sure?").setCancelable(false)
                            .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    DeletePlanning(Integer.toString(mArrayList.get(i).getIdPlanning()));
                                    Toast.makeText(c, mArrayList.get(i).getJudul() + " is deleted", Toast.LENGTH_SHORT).show();
                                }
                            })
                            .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.cancel();
                                }
                            });

                    AlertDialog alertDialog = builder.create();
                    alertDialog.show();
                    return false;
                }
            });

            viewHolder.cvPopular.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(c, DetailPlanningActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putString("idPlanning", Integer.toString(mArrayList.get(i).getIdPlanning()));
                    bundle.putString("judul", mArrayList.get(i).getJudul());
                    bundle.putString("tglPlanning", mArrayList.get(i).getTglPlanning());
                    intent.putExtras(bundle);
                    c.startActivity(intent);
                }
            });
        }

        @Override
        public int getItemCount() {
            return mArrayList.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            TextView txtWisata;
            TextView txtTanggal;
            CardView cvPopular;

            public ViewHolder(View itemView) {
                super(itemView);
                txtWisata = (TextView) itemView.findViewById(R.id.txtWisata);
                txtTanggal = (TextView) itemView.findViewById(R.id.txtTanggal);
                cvPopular = (CardView) itemView.findViewById(R.id.cvPopular);
            }
        }

        public void DeletePlanning(final String idPlanning) {
            requestQueue = Volley.newRequestQueue(c);
            String urls = c.getString(R.string.api) + "DeletePlanning.php";
            stringRequest = new StringRequest(Request.Method.POST, urls, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    try {
                        JSONObject jsonResponse = new JSONObject(response);
                        boolean success = jsonResponse.getBoolean("success");
                        JSONObject j = null;

                        if (success) {
                            listPlanning.clear();
                            adapter.notifyDataSetChanged();
                            getListPlanning();
                        } else {
                            Toast.makeText(c, "Failed!", Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(c, "Error", Toast.LENGTH_SHORT).show();
                }
            }){
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<>();
                    params.put("idPlanning", idPlanning);
                    params.put("idUser", idUser);
                    return params;
                }
            };
            requestQueue.add(stringRequest);
        }
    }
}
