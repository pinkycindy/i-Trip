package com.ppl.fikkrip.itrip.model;

/**
 * Created by Fikkri Prasetya on 19/12/2017.
 */

public class EventModel {
    String idEvent, idProvinsi, namaEvent, tglEvent, deskripsiEvent, lokasiEvent;

    public String getIdEvent() {
        return idEvent;
    }

    public void setIdEvent(String idEvent) {
        this.idEvent = idEvent;
    }

    public String getIdProvinsi() {
        return idProvinsi;
    }

    public void setIdProvinsi(String idProvinsi) {
        this.idProvinsi = idProvinsi;
    }

    public String getNamaEvent() {
        return namaEvent;
    }

    public void setNamaEvent(String namaEvent) {
        this.namaEvent = namaEvent;
    }

    public String getTglEvent() {
        return tglEvent;
    }

    public void setTglEvent(String tglEvent) {
        this.tglEvent = tglEvent;
    }

    public String getDeskripsiEvent() {
        return deskripsiEvent;
    }

    public void setDeskripsiEvent(String deskripsiEvent) {
        this.deskripsiEvent = deskripsiEvent;
    }

    public String getLokasiEvent() {
        return lokasiEvent;
    }

    public void setLokasiEvent(String lokasiEvent) {
        this.lokasiEvent = lokasiEvent;
    }
}
