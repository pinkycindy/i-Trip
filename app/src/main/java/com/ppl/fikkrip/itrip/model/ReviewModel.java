package com.ppl.fikkrip.itrip.model;

/**
 * Created by Pinky Cindy on 01/14/18.
 */

public class ReviewModel {
    private int idWisata;
    private int idReview;
    private String ulasan;
    private int idUser;
    private String username;
    private String tglReview;

    public int getIdWisata() {
        return idWisata;
    }

    public void setIdWisata(int idWisata) {
        this.idWisata = idWisata;
    }

    public int getIdReview() {
        return idReview;
    }

    public void setIdReview(int idReview) {
        this.idReview = idReview;
    }

    public String getUlasan() {
        return ulasan;
    }

    public void setUlasan(String ulasan) {
        this.ulasan = ulasan;
    }

    public int getIdUser() {
        return idUser;
    }

    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getTglReview() {
        return tglReview;
    }

    public void setTglReview(String tglReview) {
        this.tglReview = tglReview;
    }
}
