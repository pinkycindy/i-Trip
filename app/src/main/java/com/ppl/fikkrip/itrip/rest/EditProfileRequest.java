package com.ppl.fikkrip.itrip.rest;

import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Fikkri Prasetya on 13/01/2018.
 */

public class EditProfileRequest extends StringRequest {
    private Map<String, String> params;

    public EditProfileRequest(String idUser, String nama, String username, String password, String email, String url, Response.Listener<String> listener) {
        super(Method.POST, url, listener, null);
        params = new HashMap<>();
        params.put("idUser", idUser);
        params.put("nama", nama);
        params.put("username", username);
        params.put("password", password);
        params.put("email", email);
    }

    @Override
    public Map<String, String> getParams() {
        return params;
    }
}